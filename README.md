# API SWGOH Help

[![dependency status](https://deps.rs/repo/gitlab/swgoh-game/api-swgoh-help/status.svg)](https://deps.rs/repo/gitlab/swgoh-game/api-swgoh-help) 
[![pipeline status](https://gitlab.com/swgoh-game/api-swgoh-help/badges/master/pipeline.svg)](https://gitlab.com/swgoh-game/api-swgoh-help/commits/master)
[![coverage report](https://gitlab.com/swgoh-game/api-swgoh-help/badges/master/coverage.svg)](https://gitlab.com/swgoh-game/api-swgoh-help/commits/master)

Client for [swogh.help api](https://api.swgoh.help/swgoh/)

## Installation

```toml
[dependencies]
api-swgog-help = "0.1"
```

## Example

This example print all units
```rust
extern crate api_swgoh_help;

use api_swgoh_help::get_units;

fn main(){
    let units = get_units().unwrap();
    
    units.iter().foreach(|u| println!("{}", unit.name));
}
```

## Versioning

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html) 
and use [git flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) branching model.

## Roadmap

- [ ] Add get player profiles
- [ ] Add get guild profiles
- [ ] Add get events
- [ ] Add get units
- [ ] Add get squads
- [ ] Add get zetas
- [ ] Add get rooster
- [ ] Add get battles
- [ ] Add get data

## License

Licensed under

 * MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)
